
package com.s3s.mongowithopen.requestmodel;
import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.springframework.data.annotation.Id;


@Document(value = "employee-database")
@Component
@JsonInclude(value = Include.NON_NULL)
public class Employee implements Serializable{
	
	private static final long serialVersionUID = 1;
	
	@Id
	private Integer Id;
	
	private String firstName;
	
	private String lastName;

	
	@Override
	public String toString() {
		return "Employee [Id=" + Id + ", firstName=" + firstName + "]";
	}


	public Integer getId() {
		return Id;
	}


	public void setId(Integer id) {
		Id = id;
	}
	
	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	
	
	
	

}
