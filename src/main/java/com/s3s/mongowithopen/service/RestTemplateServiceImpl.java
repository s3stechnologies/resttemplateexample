package com.s3s.mongowithopen.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.s3s.mongowithopen.requestmodel.Employee;

@Service
public class RestTemplateServiceImpl {
	
	@Value("${employee.info.url}")
	private String employeeUrl;
	
	@Value("${employee.save.info.url}")
	private String saveEmployeeUrl;
	
	@Autowired
	private RestTemplate restTemplate;

	public List<Map> getEmployeeInfo() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		//List<String> response = new ArrayList<>();
	    HttpEntity <String> entity = new HttpEntity<String>(headers);
	   List<Map> response = null;
	    try {
	    	//response = restTemplate.get(employeeUrl, HttpMethod.GET, entity, List.class)
	    	 response = restTemplate.exchange(employeeUrl, HttpMethod.GET, entity, List.class).getBody();
	    	 if(null == response) {
	    		 throw new Exception();
	    	 }
	       
	    }catch(Exception e) {
	    	e.getMessage();
	    }
	    return response;
	}
	
	public String saveEmployeeInfo(List<Employee> employee) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		//this is how you send list of employees through HttpEntity
	    HttpEntity <List<Employee>> entity = new HttpEntity<List<Employee>>(employee,headers);
	    String response = null;
	    try {
	    	
	    	  response = restTemplate.exchange(saveEmployeeUrl, HttpMethod.POST, entity, String.class).getBody();
	    	  
	    	 if(null == response) {
	    		 throw new Exception();
	    	 }
	       
	    }catch(Exception e) {
	    	e.getMessage();
	    }
	    return response;
	}
	

}
