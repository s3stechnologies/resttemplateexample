package com.s3s.mongowithopen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestTempalteExample {

	public static void main(String[] args) {
		SpringApplication.run(RestTempalteExample.class, args);
	}

}
