package com.s3s.mongowithopen.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.s3s.mongowithopen.requestmodel.Employee;
import com.s3s.mongowithopen.service.RestTemplateServiceImpl;

@RestController
public class RestTemplateExampleController {
	
	@Autowired
	private RestTemplateServiceImpl service;

	
	@GetMapping(value = "/private/v1/getInfo")
	public List<Map> getInfo() {
		return service.getEmployeeInfo();
	}
	
	
	
	@PostMapping(value = "/save")
	/*Since our api :api-service/employee/v1/save from this method public ResponseEntity saveEmployeeData(@RequestBody(required = true) List<Employee> employee)
	 * takes List of employee so we need to send it as list as well. 
	 * Also, we need to add Mongo dependecy to add @Document and @Id annotation in our Employee class because it has to be same as 
	 * we have in our Employee class in mongo project
	 */
	public String saveEmployeeData(@RequestBody List<Employee> employee) {
		return service.saveEmployeeInfo(employee);
	}
}
